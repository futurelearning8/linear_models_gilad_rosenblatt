# Linear models

This is the linear models exercise for FL8 week 2 day 1 by Gilad Rosenblatt.

## Instructions

### Run

Run `main.py` in the terminal from `code` as current working directory.

### Data

Datasets are is the ```data``` directory should in the repo base directory.

## License

[WTFPL](http://www.wtfpl.net/)
