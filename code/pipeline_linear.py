import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score


class Pipeline:
    """
    Pipeline to read > clean > split > normalize > fit > score a linear model (uses scikit-learn to fit).
    Meant to run at the class (not instance) level.
    """

    # Hash the interpretation of all relevant non-empty columns (plus date and station ID).
    # Column interpretations for dataset are taken from: <https://www.kaggle.com/smid80/weatherww2>.
    column_interpretations = {
        "STA": "Weather station",  # Categorical.
        "DATE": "Date of observation",  # Datetime.
        "YR": "Year of observation",  # Categorical.
        "MO": "Month of observation",  # Categorical.
        "DA": "Day of observation",  # Categorical.
        "PRCP": "Precipitation in inches and hundredths",
        "DR": "Peak wind gust direction in tens of degrees",
        "SPD": "Peak wind gust speed in knots",
        "MEA": "Mean temperature in degrees Fahrenheit",
        "SNF": "Snowfall in inches and tenths",
        "MIN": "Minimum temperature in degrees Fahrenheit",
        "MAX": "Maximum temperature in degrees Fahrenheit"
    }

    # Path to data.
    filename = "../data/weatherww2.csv"

    # Target column names: DEFINES the optimization problem!
    targets = ["MIN", "MAX"]

    # Feature column names: UPDATES with auto-selected features during clean.
    features = []

    # The "auto feature selection" rule: threshold%+ NaNs drops a feature.
    threshold = 0.3

    # Linear regression model: UPDATES with trained model during fit.
    model = None

    @staticmethod
    def read():
        # Load the dataset into a pandas dataframe.
        df = pd.read_csv(Pipeline.filename, low_memory=False)  # FIXME DtypeWarning.

        # Shuffle in place (random but repeatable) reset index and remove empty columns.
        df = df \
            .sample(frac=1, random_state=42) \
            .reset_index(drop=True)

        # Return shuffled dataframe.
        return df

    @classmethod
    def clean(cls, df):
        # Convert date to datetime (use uppercase for consistency) and station ID to categorical.
        df["DATE"] = pd.to_datetime(df["Date"])
        df["STA"] = df["STA"].astype("category")

        # Divide columns into (relevant) quantitative and categorical.
        cols_relevant = cls.column_interpretations.keys()
        cols_categorical = ["DATE", "STA", "YR", "MO", "DA"]  # For convenience datetime masks as categorical.
        cols_quantitative = [col for col in cols_relevant if col not in cols_categorical]

        # Reduce dataframe to relevant columns (ignore duplicates and stubs).
        # df = df[cols_relevant]

        # Cast quantitative columns to float for easy digestion by numpy ops (reduce non-value 'T' to NaN).
        df[cols_quantitative] = df[cols_quantitative] \
            .replace('T', np.nan) \
            .astype(float)

        # Drop rows for which any of the targets is missing.
        bad_rows = df.index[df[cls.targets].isna().any(axis=1)]
        df.drop(index=bad_rows, inplace=True)

        # Drop columns with a high percentage of missing values.
        bad_cols = list(df.columns[df.isna().mean() > cls.threshold])  # Keep x_cols variables lists (not pandas.Index).
        df.drop(columns=bad_cols, inplace=True)

        # Select features: take the remaining quantitative columns as features for regression (update class attribute).
        cls.features = [col for col in cols_quantitative if col not in cls.targets + bad_cols]

        # Impute the remaining missing values using the monthly averages per year (makes sense).
        # NOTE: it makes more sense to groupby weather station but it is more computationally intensive and leaves NaNs.
        df[cls.features] = df[cls.features] \
            .groupby([df.DATE.dt.year, df.DATE.dt.month]) \
            .transform(lambda x: x.fillna(x.mean()))

        # Make sure no missing values were left untreated.
        assert df[cls.features].notna().all().all()

        # Extract numpy arrays for regression.
        X = df[cls.features].values
        y = df[cls.targets].values

        # Return features and targets.
        return X, y

    @classmethod
    def split(cls, X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        return X_train, X_test, y_train, y_test

    @classmethod
    def normalize(cls, X_train, X_test, y_train, y_test):
        scaler = StandardScaler()
        scaler.fit(X_train)
        return scaler.transform(X_train), scaler.transform(X_test), y_train, y_test

    @classmethod
    def fit(cls, X_train, X_test, y_train, y_test):
        cls.model = LinearRegression(fit_intercept=True, normalize=False)  # Normalize at Pipeline.normalize.
        cls.model.fit(X_train, y_train)
        return cls.model.predict(X_train), cls.model.predict(X_test), y_train, y_test

    @classmethod
    def score(cls, y_train_preds, y_test_preds, y_train, y_test):
        return r2_score(y_train, y_train_preds), r2_score(y_test, y_test_preds)

    @staticmethod
    def run():
        """Runs the pipeline read + shuffle > clean + select features > split > normalize > fit > score."""
        train_score, test_score = \
            Pipeline.score(
                *Pipeline.fit(
                    *Pipeline.normalize(
                        *Pipeline.split(
                            *Pipeline.clean(
                                Pipeline.read()
                            )
                        )
                    )
                )
            )
        return train_score, test_score

    @classmethod
    def get_model_coefficients(cls):
        theta = np.append(
            Pipeline.model.intercept_.reshape((-1, 1)),
            Pipeline.model.coef_, axis=1
        )
        return theta

    @staticmethod
    def report_model_performance(train_score, test_score, coefficients, source="LinearRegression (sklearn)"):
        print(f"--------------------------------------------------------")
        print(f"Model evaluation for {source}:")
        print(f"--------------------------------------------------------")
        print(f"Train score = {train_score}\nTest score = {test_score}")
        print(f"Model coefficients (theta):\n{coefficients}")
        print(f"--------------------------------------------------------")

    @classmethod
    def report_model_features(cls):
        print(f"--------------- Linear Regression Model ----------------")
        print(f"Targets (to predict): {cls.targets}:")
        print(f"Features (predict using): {cls.features}:")
        print(f"--------------------------------------------------------")


def main():
    """Run a pipeline to read > clean > split > normalize > fit > score a linear model using a scikit-learn."""
    train_score, test_score = Pipeline.run()
    Pipeline.report_model_performance(
        train_score=train_score,
        test_score=test_score,
        coefficients=Pipeline.get_model_coefficients()
    )


if __name__ == '__main__':
    main()
