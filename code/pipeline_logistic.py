import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


class Pipeline:
    """
    Pipeline to read > clean > split > normalize > fit > score a linear model (uses scikit-learn to fit).
    Meant to run at the class (not instance) level.
    """

    # Path to data.
    filename = "../data/data_banknote_authentication.csv"

    # Target column names.
    target = ["authenticity"]

    # Feature column names.
    features = ['variance', 'skewness', 'curtosis']

    # Logistic regression model: UPDATES with trained model during fit.
    model = None

    @staticmethod
    def read():
        return pd.read_csv(Pipeline.filename)

    @classmethod
    def clean(cls, df):
        # Drop rows for which any of the features or target is missing and shuffle in place.
        df = df\
            .dropna(subset=cls.features + cls.target)\
            .sample(frac=1, random_state=42)

        # Cast quantitative columns to float for easy digestion by numpy ops.
        df[cls.features] = df[cls.features].astype(float)

        # Extract numpy arrays for regression.
        X = df[cls.features].values
        y = df[cls.target].values

        # Return features and targets.
        return X, y

    @classmethod
    def split(cls, X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        return X_train, X_test, y_train, y_test

    @classmethod
    def normalize(cls, X_train, X_test, y_train, y_test):
        scaler = StandardScaler()
        scaler.fit(X_train)
        return scaler.transform(X_train), scaler.transform(X_test), y_train, y_test

    @classmethod
    def fit(cls, X_train, X_test, y_train, y_test):
        cls.model = LogisticRegression(fit_intercept=True)
        cls.model.fit(X_train, y_train.reshape(-1,))
        return cls.model.predict(X_train), cls.model.predict(X_test), y_train, y_test

    @classmethod
    def score(cls, y_train_preds, y_test_preds, y_train, y_test):
        return accuracy_score(y_train, y_train_preds), accuracy_score(y_test, y_test_preds)

    @staticmethod
    def run():
        """Runs the pipeline read + shuffle > clean + select features > split > normalize > fit > score."""
        train_score, test_score = \
            Pipeline.score(
                *Pipeline.fit(
                    *Pipeline.normalize(
                        *Pipeline.split(
                            *Pipeline.clean(
                                Pipeline.read()
                            )
                        )
                    )
                )
            )
        return train_score, test_score

    @classmethod
    def get_model_coefficients(cls):
        theta = np.append(
            Pipeline.model.intercept_.reshape((-1, 1)),
            Pipeline.model.coef_, axis=1
        )
        return theta

    @staticmethod
    def report_model_performance(train_score, test_score, coefficients, source="LogisticRegression (sklearn)"):
        print(f"--------------------------------------------------------")
        print(f"Model evaluation for {source}:")
        print(f"--------------------------------------------------------")
        print(f"Train score = {train_score}\nTest score = {test_score}")
        print(f"Model coefficients (theta):\n{coefficients}")
        print(f"--------------------------------------------------------")

    @classmethod
    def report_model_features(cls):
        print(f"--------------- Logistic Regression Model ----------------")
        print(f"Targets (to predict): {cls.target}:")
        print(f"Features (predict using): {cls.features}:")
        print(f"--------------------------------------------------------")


def main():
    """Run a pipeline to read > clean > split > normalize > fit > score a linear model using a scikit-learn."""
    train_score, test_score = Pipeline.run()
    Pipeline.report_model_performance(
        train_score=train_score,
        test_score=test_score,
        coefficients=Pipeline.get_model_coefficients()
    )


if __name__ == '__main__':
    main()
