"""
Simple linear regression using batch gradient descent by Gilad Rosenblatt.
Please go over __main__ for breakdown of which targets are tracked (it runs 2 cases).

NOTE: this implementation is fully-vectorized (up to epoch/batch iterations) and regresses for MULTIPLE TARGETS.
In case of k-targets m-features and n-samples matrix dimensions are as follows:

    Dimension of X (features matrix):

             feature 1   feature 2 ... feature m
    sample 1    x           x             x
    sample 2    x           x             x
    .
    .
    .
    sample n    x           x             x

    Dimension of Y (targets matrix):

             target 1   target 2 ... target k
    sample 1    x           x             x
    sample 2    x           x             x
    .
    .
    .
    sample n    x           x             x

    Dimension of Theta (model coefficients matrix):

             feature 1   feature 2 ... feature m
    target 1    x           x             x
    target 2    x           x             x
    .
    .
    .
    target k    x           x             x

The relevant formulae are:
Linear model hypothesis: H = X * Theta^T
Cost vector function: mean_over_rows( (X * Theta^T - Y)**2[element_wise_squared] ) / 2
Jacobian matrix (extending gradient vector): d[Theta]/d[X] = (X * Theta^T - Y)^T * X / n
"""

import numpy as np
from pipeline_linear import Pipeline


def hypothesis(X, theta):
    """
    :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
    :param np.ndarray theta: model coefficients.
    :return np.ndarray H: model's output on the sample set.
    """
    return np.matmul(X, theta.T)


def cost(X, theta, y):
    """
    :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
    :param np.ndarray theta: model coefficients.
    :param np.ndarray y: target vector (ground truth for each sample).
    :return np.ndarray: the model's mean square error loss over all samples (rows-1, cols-targets).
    """
    return np.mean(np.power(hypothesis(X, theta) - y, 2), axis=0) / 2


def gradient(X, theta, y):
    """
    :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
    :param np.ndarray theta: model coefficients.
    :param np.ndarray y: target vector (ground truth for each sample).
    :return np.ndarray gradient: coefficients gradients (Jacobian matrix) for current step.
    """
    return np.matmul((hypothesis(X, theta) - y).T, X) / X.shape[0]


def batch_gradient_descent(X, theta, y, learning_rate, batch_size, num_epochs):
    """
    :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
    :param np.ndarray theta: model coefficients.
    :param np.ndarray y: target vector (ground truth for each sample).
    :param float learning_rate: learning rate.
    :param int batch_size: training batch size per iteration.
    :param int num_epochs: total number of passes through the data.
    :return np.ndarray theta: optimized model coefficients.
    """

    # Determine how many steps are needed per epoch.
    num_samples = X.shape[0]
    num_steps = num_samples // batch_size + 1

    # Start a loop over the number of epochs.
    for epoch_num in range(num_epochs):

        # Start a loop over the number of steps required to finish an epoch
        for step_num in range(num_steps):

            # Fetch the next batch from the data
            start = step_num * batch_size
            finish = start + batch_size  # NOTE: Slicing above index range cuts slice without raising an exception.

            # Compute the gradient for this batch and update model coefficients according to learning rate.
            theta -= learning_rate * gradient(X[start:finish], theta, y[start:finish])

        # Print the cost every epoch.
        # NOTE: loss can be a vector if there are multiple targets.
        loss = cost(X, theta, y)
        print(f"Epoch {epoch_num + 1}: loss = {loss}")  # Count epochs starting at 1.

    # Return the optimized model coefficients.
    return theta


def main():

    # Load the dataset, shuffle, clean, split, and normalize (StandardScaler) using the Pipeline class.
    X_train, X_test, y_train, y_test = \
        Pipeline.normalize(
            *Pipeline.split(
                *Pipeline.clean(
                    Pipeline.read()
                )
            )
        )
    #

    # Add bias column to train and test sets.
    X_train = np.append(np.ones((X_train.shape[0], 1)), X_train, axis=1)
    X_test = np.append(np.ones((X_test.shape[0], 1)), X_test, axis=1)

    # Randomly initialize theta (model coefficients).
    num_cols = 1 + len(Pipeline.features)  # BIAS + number of features
    num_rows = len(Pipeline.targets)  # number of targets
    theta = np.random.random((num_rows, num_cols))

    # Configure training hyperparameters.
    learning_rate = 0.3
    batch_size = 3000
    num_epochs = 30

    # Fit the model on the training set and get optimized model coefficients (my_theta).
    my_theta = batch_gradient_descent(X_train, theta, y_train, learning_rate, batch_size, num_epochs)

    # Score the model on the train and test sets.
    my_train_score, my_test_score = \
        Pipeline.score(
            hypothesis(X_train, my_theta),
            hypothesis(X_test, my_theta),
            y_train,
            y_test
        )

    # Now solve the same optimization problem using a scikit-learn LinearRegression model (implemented in Pipeline).
    sklearn_train_score, sklearn_test_score = Pipeline.run()
    sklearn_theta = Pipeline.get_model_coefficients()

    # Print comparison between model outcomes to console.
    print()  # White space line.
    Pipeline.report_model_features()
    Pipeline.report_model_performance(
        train_score=my_train_score,
        test_score=my_test_score,
        coefficients=my_theta,
        source="my linear regression model"
    )
    Pipeline.report_model_performance(
        train_score=sklearn_train_score,
        test_score=sklearn_test_score,
        coefficients=sklearn_theta,
        source="LinearRegression (sklearn)"
    )


if __name__ == '__main__':

    # Let's first predict the maximum temperature using the minimum temperate (plus some more features).
    Pipeline.targets = ["MAX"]
    main()

    # Now let's predict both the maximum and minimum temperatures using the other features.
    Pipeline.targets = ["MAX", "MIN"]
    main()
